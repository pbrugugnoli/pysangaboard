.. pysangaboard documentation master file, created by
   sphinx-quickstart on Wed May 22 11:07:35 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pysangaboard's documentation!
========================================

This module exposes the functions of the "Sangaboard" in a
friendly Python class.  The Sangaboard is used for driving cheap
unipolar stepper motors such as the  28BYJ-48. Functions are provided
to move the motors, as well as properties that allow motion to be
configured.  Various context managers and generator functions are
provided to simplify opening/closing the hardware.

All of the functionality is accessed through the :class:`sangaboard.Sangaboard` class.
As the Sangaboard firmware can be modified to have support for light sensors and/or
endstops, the availability of optional modules can be checked with
:meth:`sangaboard.Sangaboard.list_modules`. Possible modules include
:class:`sangaboard.sangaboard.LightSensor` and  :class:`sangaboard.sangaboard.Endstops`.
Endstop support requires v0.3 of the Sangaboard, and the light sensor requires
a supported light sensor connected over I2C.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   hardware
   getting_started
       
       
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

********************
Sangaboard class
********************

.. autoclass:: sangaboard.Sangaboard
    :members:
        
****************************
Sangaboard optional modules
****************************

#########
Endstops
#########

.. autoclass:: sangaboard.sangaboard.Endstops
    :members:
        
#############
Light Sensor
#############
.. autoclass:: sangaboard.sangaboard.LightSensor
    :members:




